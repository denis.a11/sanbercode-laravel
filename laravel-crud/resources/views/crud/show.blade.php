@extends('master')
@section('content')
<div class="col-sm-6">
    <h1>Detail Data Cast</h1>
</div>
<div class="card">
    <div class="card-header">
      <h1 class="card-title"></h1>
    </div>
    <div class="card-body">
        <h1>{{$cast->nama}}</h1>
        <p>{{$cast->bio}}</p>
    </div>
    <h1><a href="/cast" class="btn btn-secondary ml-3">Kembali</a></h1>
</div>
@endsection

