@extends('master');
@section('content')

<a href="/cast/create" class="btn btn-primary ml-3 mb-3">Tambah Cast</a>
<table class="ml-3 table table-striped">
    <thead>
      <tr>
        <th scope="col">No</th>
        <th scope="col">Nama</th>
        <th scope="col">Umur</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
    {{-- Gabungan looping foreach dan if else  --}}

      @forelse ($cast as $key => $item)
        <tr>
            <td>{{$key + 1}}</td>
            <td>{{$item->nama}}</td>
            <td>{{$item->umur}}</td>
            <td>
                
                <form action="/cast/{{$item->id_cast}}" method="POST">
                    @csrf
                    <a href="/cast/{{$item->id_cast}}" class="btn btn-info btn-sm">Detail</a>
                <a href="/cast/{{$item->id_cast}}/edit" class="btn btn-warning btn-sm">Edit</a>
                    @method('delete')
                    <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                </form>
            </td>   
        </tr>
          
      @empty {{-- tampilkan jika tidak ada --}}
        <tr>
                <td>Data Masih Kosong</td>
        </tr>
          
      @endforelse
    </tbody>
  </table>

@endsection;