@extends('master');

@section('content')


<form action="/cast" method="POST">
    @csrf
    <div class="card-header">
        <h3 class="card-title">Data Pemain Film</h3>
      </div>
    <div class="card-body">
      <div class="form-group">
        <label for="nama">Nama Cast</label>
        <input type="text" name="nama" class="form-control" id="nama" placeholder="Masukkan Nama">
      </div>
      @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
      @enderror
      <div class="form-group">
        <label for="umur">Umur</label>
        <input type="text" name="umur" class="form-control" id="umur" placeholder="Masukkan Umur">
      </div>
      @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
      @enderror
    <div class="form-group">
      <label for="bio">Biodata</label>
      <textarea class="form-control" name="bio" rows="7" id="bio" placeholder="Masukkan Biodata"></textarea>
    </div>
    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
  </div>
    <!-- /.card-body -->
    <div class="card-footer">
      <button type="submit" class="btn btn-primary">Create</button>
    </div>
  </form>
  @endsection;