<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function create(){
        return view('crud.create');
    }
    //Validasi data yang mau di masukkan ke database
    public function store(Request $request){
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ]);
    //Masukkan data ke database
    DB::table('cast')->insert(
        [
        'nama' => $request['nama'],
        'umur' => $request['umur'],
        'bio' => $request['bio']
        ]
    );
    //mengembalikan atau kembali ke halaman cast/create
    return redirect('/cast');
    }
    public function index(){
        $cast = DB::table('cast')->get();
        return view('crud.index', compact('cast'));
        //compact artinya melemparkan/mengirim variable
    }
    public function show($id){
        $cast = DB::table('cast')->where('id_cast',$id)->first();
        return view('crud.show',compact('cast'));
    }
    public function edit($id){
        $cast = DB::table('cast')->where('id_cast',$id)->first();
        return view('crud.edit',compact('cast'));            
        }

    public function update(Request $request, $id){
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ]);

        $affected = DB::table('cast')
        ->where('id_cast', $id)
        ->update(
            [
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'bio' => $request['bio']
        ]);
        return redirect('/cast');
    }
    public function destroy($id){
    $cast = DB::table('cast')->where('id_cast','=',$id)->delete();
    return redirect('/cast');
    }
}
