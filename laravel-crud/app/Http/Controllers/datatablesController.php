<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class datatablesController extends Controller
{
    public function table(){
        return view("items.table");
    }
    public function datatables(){
        return view("items.datatables");
    }
    public function master(){
        return view("master");
    }
}
