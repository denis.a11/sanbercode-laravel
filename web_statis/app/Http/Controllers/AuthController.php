<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view("registrasi");
    }
    public function welcome(Request $request){
        $namadepan = strtoupper($request['namadepan']);
        $namabelakang = strtoupper($request['namabelakang']);
        return view(("welcome"), compact("namadepan","namabelakang"));
    }
}