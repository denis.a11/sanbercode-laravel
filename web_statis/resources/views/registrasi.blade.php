<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    <form action="/welcome" method="POST">
        @csrf
        <!-- Nama-->
        <label>First Name : </label> <br>
        <input type="text" name="namadepan" required><br><br>
        <label>Last Name : </label><br>
        <input type="text" name="namabelakang" require><br><br>

        <!--Gender-->
        <label>Gender : </label><br>
        <input type="radio" name="gender" value="male">
        <label for="male">Male</label><br>
        <input type="radio" name="gender" value="female">
        <label for="female">Female</label><br>
        <input type="radio" name="gender" value="other">
        <label for="other">Other</label><br><br>

        <!--Kebangsaan-->
        <label>Nationality : </label> <br>
        <select>
            <option>Indonesia</option>
            <option>Inggris</option>
            <option>Thailand</option>
            <option>China</option>
        </select><br><br>

        <!--Bahasa-->
        <label>Languange Spoken : </label><br>
        <input type="checkbox" name="indonesia" value="indonesia">
        <label for="indonesia">Indonesia</label><br>
        <input type="checkbox" name="china" value="china">
        <label for="china">China</label><br>
        <input type="checkbox" name="inggris" value="inggris">
        <label for="inggris">Inggris</label><br><br>

        <!--Text Area-->
        <label>Bio : </label><br>
        <textarea cols="30" rows="7" placeholder="Tuliskan Bio Disini"></textarea><br>
        <button type="submit">Sign Up</button>
        
    </form>
</body>
</html>